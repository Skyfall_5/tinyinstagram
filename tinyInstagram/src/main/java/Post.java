
import java.util.LinkedList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Post {

	@Id
	private String id; 
	
	@Index
	private String sender;
	private String message;
	private String photo;
	private List<Likes> likes;
	@Index
	private String date;
	
	public Post() {}
	
	public Post(String id, String sender, String photo, String message, String date) {
		this.id = id;
		this.sender = sender;
		this.photo = photo;
		this.message = message;
		this.likes = new LinkedList<>();
		this.date = date;
		
	}
	
	public String getSender() {
		return this.sender;
	}
	
	public void setSender(String sender) {
		this.sender = sender;
	}
	
	public String getDate(){
		return this.date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getPhoto() {
		return(this.photo);
	}
	
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	public String getMessage() {
		return(this.message);
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public List<Likes> getLikes() {
		return(this.likes);
	}
	
	public void removePost() {
		this.photo = null;
		this.message = null;
		this.likes.clear();
	}
}
