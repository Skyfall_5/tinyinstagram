import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.LinkedList;
import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

public class LikesRepository {
	
	private static LikesRepository likesRep = null;
	
	static {
		ObjectifyService.register(Person.class);
	}
	
	private LikesRepository() {
	}
	
	public static synchronized LikesRepository getInstance() {
		if (null == likesRep) {
			likesRep = new LikesRepository();
		}
		return likesRep;
	}
	
	public Likes createLike(Likes likes) {
		ofy().save().entity(likes).now();
		return likes;
	}
	
	public Likes updateLikes(Likes likes) {
		if (likes.getId() == null) {
			return null;
		}
		Likes updatedLikes = ofy().load().type(Likes.class).id(likes.getId()).now();
		if (likes.getLiker() != null) {
			updatedLikes.setLiker(likes.getLiker());
		}
		if (likes.getLikes() != 0) {
			updatedLikes.setLikes(likes.getLikes());
		}
		ofy().save().entity(updatedLikes).now();
		return likes;
	}
	
	public void removeLike(String id) {
		if (id == null) {
			return;
		}
		ofy().delete().type(Likes.class).id(id).now();
	}
	
	public Likes findLikes(String id) {
		Likes LikeSearched = ofy().load().type(Likes.class).id(id).now();
		if (LikeSearched == null) {
			return null;
		} else {
			return LikeSearched;
		}
	}
	
	public void deleteAllLikes() {
		Iterable<Key<Likes>> likes = ofy().load().type(Likes.class).keys();
		ofy().delete().keys(likes);
	}
}
