
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Person {

	@Id
	private String id;
	
	@Index
	private String name;
	private String description;
	//private List<Post> posts;
	private List<Person> followers;
	private List<Person> followings;
	
	public Person(){}
	
	public Person(String id, String name, String description) {
		this.id = id;
		this.name = name;
		this.description = description;
		//this.posts = new LinkedList<>();
		this.followers = new ArrayList<>();
		this.followings = new ArrayList<>();
		
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return(this.description);
	}
	
	public boolean addFollowing(Person p) {
		return(this.followings.add(p));
	}
	
	public List<Person> getFollowingList(){
		return(this.followings);
	}
	
	public void setFollowingList(List<Person> followings) {
		this.followings = followings;
	}
	
	public Person getFollowing(int i) {
		return(this.followings.get(i));
	}
		
	public boolean removeFollowing(Person p) {
		return(this.followings.remove(p));
	}
	
	public List<Person> getFollowers(){
		return(this.followers);
	}
	
	public void setFollowers(List<Person> followers) {
		this.followers = followers;
	}
	
	public boolean addFollower(Person follower) {
		return(this.followers.add(follower));
	}
	
	
}
