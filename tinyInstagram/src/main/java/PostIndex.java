import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

@Entity
public class PostIndex {

	@Id
	private String id;
	@Parent
	private Key<Post> post;
	@Index
	private String sender;
	@Index
	private List<String> recevers;
	@Index
	private String date;
	
	public PostIndex() {}
	
	public PostIndex(String id, String sender, List<String> recevers, Key<Post> post, String date) {
		this.id =id;
		this.post = post;
		this.sender = sender;
		this.recevers = recevers;
		this.date = date;
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getSender() {
		return this.sender;
	}
	
	public void setSender(String sender) {
		this.sender = sender;
	}
	
	public List<String> getRecevers(){
		return this.recevers;
	}
	
	public void setReceveurs(List<String> recevers) {
		this.recevers = recevers;
	}
	
	public String getDate() {
		return this.date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
}
