/*import java.util.ArrayList;
import java.util.Collection;
import java.util.List;*/

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class PostRep {
	
	private static PostRep postRep = null;

	static {
		ObjectifyService.register(Post.class);
	}

	private PostRep() {
	}

	public static synchronized PostRep getInstance() {
		if (null == postRep) {
			postRep = new PostRep();
		}
		return postRep;
	}

	public Post createPost(Post post) {
		ofy().save().entity(post).now(); // enregistre immédiatement dans le datastore
		return post;
	}

	public Post updatePost(Post post) {
		if (post.getId() == null) {
			return null;
		}
		Post newPost = ofy().load().type(Post.class).id(post.getId()).now();
		if (post.getDate() != null) {
			newPost.setDate(post.getDate());
		}
		if (post.getMessage() != null) {
			newPost.setMessage(post.getMessage());
		}
		if (post.getSender() != null) {
			newPost.setSender(post.getSender());
		}
		ofy().save().entity(newPost).now(); // modifie le post existant avec les nouvelles données
		return newPost;
	}
	
	public void removePost(String id) {
		if (id == null) {
			return;
		}
		ofy().delete().type(Post.class).id(id).now();
	}
	
	public Post findPost(String id) {
		Post postSearched = ofy().load().type(Post.class).id(id).now();
		if (postSearched == null) {
			return null;
		} else {
			return postSearched;
		}
	}
	
	public void removePostUser(String id) {
		Iterable<Key<Post>> posts = ofy().load().type(Post.class).filter("sender", id).keys();
		ofy().delete().keys(posts);
	}
	
	public void deleteAllPosts() {
		Iterable<Key<Post>> posts = ofy().load().type(Post.class).keys();
		ofy().delete().keys(posts);
	}
	
}
