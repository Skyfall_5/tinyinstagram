import java.util.LinkedList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Likes {
	
	@Id
	private String id;
	
	@Index
	private int likes;
	private List<Person> liker;
	
	public Likes() {}
	
	public Likes(String id, List<Person> liker) {
		this.id = id;
		this.likes = 0;
		this.liker = liker;
		
	}
	
	// Constructeur étrange je trouve car on instancie un nombre de likes
	public Likes(String id, int likes, List<Person> liker) {
		this.id = id;
		this.likes = likes;
		this.liker = new LinkedList<>();
	}
	
	public void setLike() {
		this.likes ++;
	}
	
	public void setLikes(int likes) {
		this.likes = likes;
	}
	
	public void removeLike(Person liker) {
		if (this.likes > 0 && this.liker.contains(liker)) {
			this.likes --;
			this.liker.remove(liker);
		}
	}
	
	public boolean addLiker(Person liker) {
		return this.liker.add(liker);
	}
	
	public boolean removeLiker(Person liker) {
		return this.liker.remove(liker);
	}
	
	public String getId() {
		return this.id;
	}
	
	public List<Person> getLiker() {
		return this.liker;
	}
	
	public int getLikes() {
		return this.likes;
	}
	
	public boolean setLiker(Person liker) {
		return this.liker.add(liker);
	}
	
	public void setLiker(List<Person> likers) {
		this.liker = likers;
	}
}
