
import java.util.Collection;
import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

import static com.googlecode.objectify.ObjectifyService.ofy;


public class PersonRep {
	private static PersonRep personRep = null;

	static {
		ObjectifyService.register(Person.class);
	}

	private PersonRep() {
	}

	public static synchronized PersonRep getInstance() {
		if (null == personRep) {
			personRep = new PersonRep();
		}
		return personRep;
	}
	
	public Collection<Person> findPersonsWithName(String name) {
		List<Person> persons = ofy().load().type(Person.class).filter("name >=", name).filter("name <", name).list();
		return persons;
	}
	
	public Person findPerson(String id) {
		Person person = ofy().load().type(Person.class).id(id).now();
		if (person == null) {
			return null;
		} else {
			return person;
		}
	}

	public Person createPerson(Person person) {
		ofy().save().entity(person).now();
		return person;
	}

	public Person updatePerson(Person person) {
		if (person.getId() == null) {
			return null;
		}
		Person updatedPerson = ofy().load().type(Person.class).id(person.getId()).now();
		if (person.getName() != null) {
			updatedPerson.setName(person.getName());
		}
		if (person.getFollowers() != null) {
			updatedPerson.setFollowers(person.getFollowers());
		}
		if (person.getFollowingList() != null) {
			updatedPerson.setFollowingList(person.getFollowingList());
		}
		ofy().save().entity(updatedPerson).now();
		return person;
	}
	
	
	public void removePerson(String id) {
		if (id == null) {
			return;
		}
		ofy().delete().type(Person.class).id(id).now();
	}
	
	public void deleteAllPersons() {
		Iterable<Key<Person>> persons = ofy().load().type(Person.class).keys();
		ofy().delete().keys(persons);
	}
}
